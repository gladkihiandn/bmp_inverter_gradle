package ru.bmstu.iu4

import java.io.File

class bmp_program
{
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            if(args.size < 2) {
                println("Not enough arguments!")
                println("<BMP File Name> <BMP Inverted file name>")
                System.exit(-1)
            }
            val File = File(args[0])
            val Parcer = bmpFileParser(File)
            Parcer.invertBMP()
            Parcer.saveNewBMP(args[1])
        }
    }
}